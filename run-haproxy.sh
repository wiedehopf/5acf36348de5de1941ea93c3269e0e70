#!/bin/bash

OUTPUT=run-haproxy.log
echo writing log to $OUTPUT

run=0
while sleep 0.00001; do
  echo Run $(( ++run )): starting haproxy
  echo ---------------------------------------------------
  /usr/local/sbin/haproxy -W -f /etc/haproxy/haproxy.cfg -p /run/haproxy.pid -S /run/haproxy-master.sock
  echo ---------------------------------------------------
  echo Run $(( run )): exited
done 2>&1 | perl -pe 'use POSIX strftime; use Time::HiRes gettimeofday;
                    $|=1;
                    select((select(STDERR), $| = 1)[0]);
                    ($s,$ms)=gettimeofday();
                    $ms=substr(q(000000) . $ms,-6);
                    print strftime "[%Y-%m-%d %H:%M:%S.$ms]", localtime($s)' > $OUTPUT
