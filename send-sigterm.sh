#!/bin/bash
set -e

function mainpid() {
  { cat /run/haproxy.pid || true ; } 2>/dev/null
}

cycles=0
sleep_prefix=0.0001

#while sleep "${sleep_prefix}${RANDOM}"; do
while true; do

  OLDPID=$(mainpid)

  if (( OLDPID > 0 )); then
    # echo "Stopping haproxy $OLDPID"
    kill "$OLDPID" || true
  fi

  k=0
  while true; do
    if (( ++k > 5000 )); then
      echo "-------------------------"
      echo "race hit: did not terminate quick enough"
      echo "cycle counter: $cycles"
      exit 1
    fi
    sleep "${sleep_prefix}${RANDOM}"
    PID=$(mainpid)
    if (( PID != OLDPID || PID == 0 )); then
      break
    else
      #echo "waiting"
      true
    fi
  done

  (( ++ cycles ))
  if (( cycles % 1000 == 0 )); then
    echo cycle counter: $(( cycles ))
  fi
done
